---
layout: page
title: Datenschutzhinweise
---
Cookies, Datenschutz

## Datenschutzerklärung ##

Ich freue mich über Ihr Interesse an meiner Website. Datenschutz hat einen hohen Stellenwert für mich. Die Nutzung meiner Website ist in der Regel ohne Angabe personenbezogener Daten möglich. Es gibt jedoch Ausnahmen, über die ich Sie hiermit informieren möchte:

## Cookies ##
Meine Website verwendet i.d.R. keine Cookies (kleine Dateien, die von Ihrem Browser beim Besuch von Seiten auf Ihrem Endgerät gespeichert werden). Lediglich für die Terminvergabe (über Nextcloud) werden Cookies verwendet, um die Sicherheit der Sitzungen zu gewährleisten.

## Server-Log-Dateien ##
Die Website wird auf gitlab.com gehostet. Ich habe keinen Zugriff auf die Server-Log-Dateien und auch keine Möglichkeit, diese Daten einzusehen oder zu verarbeiten. Für weitere Informationen zur Datenverarbeitung durch gitlab.com verweise ich auf die Datenschutzerklärung von GitLab.

## Datenschutz bei der Nutzung von Nextcloud ##

Für die Terminvergabe nutze ich Nextcloud, eine Software zur Datensynchronisation und Online-Zusammenarbeit, wovon lediglich die Kalenderfunktion zur Anwendung kommt. Diese Software wird auf meinem eigenen Server gehostet um die Datensicherheit zu erhöhen. Die Software ist speziell für sicheren Datenaustausch konzipiert und die Verbindung zu diesem Dienst ist SSL-verschlüsselt, um die Sicherheit Ihrer Daten zu gewährleisten.

## Datenverarbeitung durch meinen Server bei online-Terminvergabe ##

Beim Aufrufen der Terminvergabe-Funktion auf meiner Webseite wird mein Server Informationen über Ihren Besuch erfassen, darunter Ihre IP-Adresse, Browser-Informationen und die von Ihnen eingegebenen Daten zur Terminregistrierung. Ich verwende diese Daten ausschließlich zur Bereitstellung von Terminen. Es findet keinerlei weitere Nutzung statt. 

## Rechtsgrundlage und Zweck ##

Die Nutzung der automatisierten Terminvergabe auf meiner Webseite erfolgt im Interesse einer effizienten und sicheren Terminvergabe. Dies stellt ein berechtigtes Interesse im Sinne des Art. 6 Abs. 1 lit. f DSGVO dar.

## Datenschutzeinstellungen und Widerspruchsrecht ##

Sie haben die Möglichkeit, die Datenschutzeinstellungen auf meinem Server anzupassen, sofern dies technisch umsetzbar ist. Bei Fragen zur Verarbeitung Ihrer personenbezogenen Daten durch Nextcloud können Sie sich jederzeit an mich wenden.

## Einwilligung und Widerruf ##

Sofern ich für bestimmte Vorgänge der Datenverarbeitung Ihre Einwilligung einhole, dient Art. 6 Abs. 1 lit. a DSGVO als Rechtsgrundlage für die Datenverarbeitung. Eine erteilte Einwilligung kann jederzeit widerrufen werden. Der Widerruf der Einwilligung berührt jedoch nicht die Rechtmäßigkeit der bis zum Widerruf erfolgten Verarbeitung.

## Datenschutzbeauftragter und Kontaktaufnahme ##

Für Fragen zum Datenschutz oder zur Ausübung Ihrer Rechte bezüglich Ihrer personenbezogenen Daten können Sie sich jederzeit an mich wenden. Die Kontaktdaten finden Sie im Impressum meiner Webseite.